function search(keyword){
    document.getElementById("events").innerHTML = "<h3>Searching..</h3>";
    $.ajax({
        url: "/get/event?search="+keyword,
        success: function(result){
            document.getElementById("events").innerHTML = "";
            JSON.parse(result).forEach(e => {
                document.getElementById("events").innerHTML += format(e.fields, e.pk);
            });
            if(JSON.parse(result).length == 0){
                document.getElementById("events").innerHTML = "<h3>Event Not Found</h3>";
            }
        }
    })
}

$('#search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search(event.target.value);
    }
});


function format(e, id){
    return(
        `<a href="/events/${id}">`+
        `<div class='event' style="background-image: url('media/${e.image}')">`+
        `<h4>${e.event}</h4></div></a>`
        )
}