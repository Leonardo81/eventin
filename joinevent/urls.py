from django.urls import path
from . import views


app_name = 'join'

urlpatterns = [
    path('<int:id>', views.Join, name='registrasi'),
    path('<int:id>/json-event', views.get_data, name='json-data'),
]
