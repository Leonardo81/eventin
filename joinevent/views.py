from django.shortcuts import render, redirect
from .models import Participant as peserta
from .forms import ParticipantRegistration
from django.core import serializers
from django.http import HttpResponse
from registerevent.models import EventRegis
    
def Join(request, id):
    if request.method == "POST":
        if request.user.is_authenticated:
            form = ParticipantRegistration(request.POST)
            if form.is_valid():
                p = peserta()
                p.Email = form.cleaned_data['Email']
                p.High_School_Student = form.cleaned_data['High_School_Student']
                p.Undergraduate_Student = form.cleaned_data['Undergraduate_Student']
                p.Fresh_Graduate = form.cleaned_data['Fresh_Graduate']
                p.Other = form.cleaned_data['Other']
                p.Company = form.cleaned_data['Company']
                p.Name = form.cleaned_data['Name']
                p.Gender = form.cleaned_data['Gender']
                p.Birthdate = form.cleaned_data['Birthdate']
                p.Contact = form.cleaned_data['Contact']    
                p.Privacy = form.cleaned_data['Privacy']
                p.user = request.user   
                p.event = EventRegis.objects.get(id=id)     
                p.save()
            return redirect('/')
        return redirect('account:login')
    else:
        p = peserta.objects.all()
        form = ParticipantRegistration()
        response = {"p":p, 'form' : form}
        return render(request,'registrasi.html',response)

def get_data(request, id):
    data = peserta.objects.all()
    list_data = serializers.serialize('json', data)
    return HttpResponse(list_data, content_type='text/json-comment-filtered')    

