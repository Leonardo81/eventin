from django.test import TestCase
from django.test import Client
from django.urls import resolve
from joinevent.views import Join
from django.http import HttpRequest
from registerevent.models import EventRegis

class ParticipantTest(TestCase):
    def test_participant_url_exist(self):
        response = Client().get('/join/1')
        self.assertEqual(response.status_code, 200)

    def test_notexist_url_is_notexist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 404)

    def test_participant_views_function(self):
        response = resolve('/join/1')
        self.assertEqual(response.func, Join)

    def test_participant_using_participant_template(self):
        response = Client().get('/join/1')
        self.assertTemplateUsed(response, 'registrasi.html')

    def test_participant_views(self):
        event = EventRegis.objects.create(
            location = "jkt",
            corporate_name = "name",
            address = "add",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "lalala")
        request = HttpRequest()
        response = Join(request, 1)
        self.assertContains(response, '')
