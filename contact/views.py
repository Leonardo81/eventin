from django.shortcuts import render,redirect
from .forms import contactForm
from .models import Contact
from django.http import HttpResponse
from django.core import serializers

def contact(request):
    form_contact = contactForm(request.POST or None)

    if request.method =='POST': 
        if form_contact.is_valid():
            form_contact.save()
    context = {
        'form_contact' : form_contact
    }
    return render(request,'contact.html',context)

def getDataContact(request):
    datamauJSON = Contact.objects.all()
    #dijadikanLIst
    ListData = serializers.serialize('json', datamauJSON)
    return HttpResponse(ListData, content_type="text/json-comment-filtered")