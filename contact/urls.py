from django.urls import path
from . import views

urlpatterns = [
    path('', views.contact, name="contact"),
    path('jsonContact', views.getDataContact ,name = 'getDataContact'),
]


