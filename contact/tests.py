from django.test import TestCase
from django.urls import resolve
from .views import contact
from .models import Contact
from .forms import contactForm

class ContactPageTest(TestCase):
    def test_contact_url_exist(self):
        response = self.client.get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_contact_templates(self):
        response = self.client.get('/contact/')
        self.assertTemplateUsed(response,'contact.html')

    def test_contact_using_contact_func(self):
        found = resolve('/contact/')
        self.assertEqual(found.func, contact)

    def test_model_can_create_contactus(self):
        new_contactus = Contact.objects.create(name='kim nam joon', email='namjoon@gmail.com', location='koriyah', message='kamsahamnida :)')
        counting_contact_object = Contact.objects.all().count()
        self.assertEqual(counting_contact_object,1)
