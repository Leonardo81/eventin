from django import forms

from .models import Contact

class contactForm(forms.ModelForm):
    class Meta :
        model   = Contact
        fields  = [
            'name',
            'email',
            'location',
            'about',
            'message',
        ]

        widgets = {
            'name' : forms.TextInput(
                attrs={
                    'id':'name',
                    'class' : 'form-control',

                }
            ),

            'email' : forms.TextInput(
                attrs={
                    'id':'email',
                    'class' : 'form-control',
                    'placeholder' :'@gmail.com'
                }

            ),

            'location' : forms.TextInput(
                attrs={
                    'id':'location',
                    'class' : 'form-control',
                }

            ),

            'about' : forms.Select(
                attrs={
                    'id':'about',
                    'class' : 'form-control',
                }
            ),

            'message' : forms.Textarea(
                attrs={
                    'id':'message',
                    'class' : 'form-control',
                }
            ),

        }