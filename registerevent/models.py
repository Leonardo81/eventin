from django.db import models
from django.utils import timezone

# Create your models here.
class EventRegis (models.Model):
	location = models.CharField(max_length=50)
	corporate_name = models.CharField(max_length=50)
	address = models.CharField(max_length=50)
	email = models.EmailField(max_length=50)
	event = models.CharField(max_length=50)
	tanggal = models.DateField()
	waktu = models.TimeField()	
	image = models.ImageField(upload_to="Images", default="Images/default.jpeg")
	desc = models.TextField(max_length = 1000)
	