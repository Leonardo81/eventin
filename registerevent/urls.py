from django.urls import path
from . import views

urlpatterns = [
	path('', views.event_form, name='pendaftaran'),
	path('json-event', views.get_data_event_regis, name='json-data'),
]