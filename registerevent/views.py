from django.shortcuts import render
from .forms import EventForm
from .models import EventRegis
from django.shortcuts import redirect
from django.core import serializers
from django.http import HttpResponse

def event_form(request):
	pendaftaran_form = EventForm()
	if request.method == "POST" :
		pendaftaran_form = EventForm(request.POST, request.FILES)
		if pendaftaran_form.is_valid():
			print (request.POST)
			pendaftaran_form.save()
			
	context =  {
		'form' : pendaftaran_form
	}
	return render(request, 'event_form.html', context)

def get_data_event_regis(request):
	data = EventRegis.objects.all()
	list_data = serializers.serialize('json', data)
	return HttpResponse(list_data, content_type='text/json-comment-filtered')
# Create your views here.
