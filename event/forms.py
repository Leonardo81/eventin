from django import forms
from django.forms import TextInput
from . import models


class CreateComment(forms.ModelForm):
    class Meta:
        model = models.EventComment
        fields = ['comment']

    # def __init__(self, *args, **kwargs):
    #     super(CreateComment, self).__init__(*args, **kwargs)
    #     self.fields['comment'].widget = TextInput(attrs={
    #         'id': 'fieldName',
    #         'class': 'site-form',
    #     })