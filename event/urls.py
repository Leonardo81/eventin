from django.urls import path
from . import views


app_name = 'events'

urlpatterns = [
    path('<int:id>', views.events, name='gallery'),
    path('<int:id>/comment', views.eventComment, name='comment')
]