from django.shortcuts import render, redirect
from .forms import CreateComment
from registerevent.models import EventRegis
from django.http import JsonResponse
from .models import EventComment
from django.core.serializers import serialize


def events(request,id):
    events = EventRegis.objects.get(id=id)
    if request.method == 'POST':
        if request.user.is_authenticated:
            # print('testtt')
            form = CreateComment(request.POST, request.FILES)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.author = request.user
                instance.eventRegis = events
                instance.save()
                return redirect('events:gallery', id)
        return redirect('account:login')
    else:
        form = CreateComment()
    return render(request, 'gallery.html', {'form': form, 'event': events})

def eventComment(request,id):
    events = EventRegis.objects.get(id=id)
    comment = EventComment.objects.filter(eventRegis=events)
    data = serialize('json', comment)
    # serializers.serialize('json', posts)
    return JsonResponse(data, safe=False)



    