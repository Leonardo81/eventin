from django.db import models
from django.contrib.auth.models import User
from registerevent.models import EventRegis

# Create your models here.
class EventComment(models.Model):
    comment = models.TextField(blank = True, null = True, max_length = 500)
    author = models.ForeignKey(User, default=None, on_delete=models.PROTECT)
    eventRegis = models.ForeignKey(EventRegis, default=None, on_delete=models.PROTECT)
    