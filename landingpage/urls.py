from django.urls import path
from . import views


app_name = 'gethome'

urlpatterns = [
    path('event', views.events),
]
