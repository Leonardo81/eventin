from django.shortcuts import render,redirect
from registerevent.models import EventRegis
from .forms import FeedbackForm
from django.http import JsonResponse
from django.core.serializers import serialize
from django.db.models import Q

def landing(request):
    events = EventRegis.objects.all()
    form = FeedbackForm()
    if request.method == "POST":
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('home')
    return render(request, 'landing.html', {'events': events, 'form':form})

def events(request):
    search_key = request.GET['search']
    # events = EventRegis.objects.filter(event=search_key)
    events = EventRegis.objects.filter(
        Q(event__contains=search_key) |
        Q(location__contains=search_key) |
        Q(corporate_name__contains=search_key) |
        Q(desc__contains=search_key)
        )
    data = serialize('json', events)
    return JsonResponse(data, safe=False)