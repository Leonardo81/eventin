from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest
from .models import Feedback
from .forms import FeedbackForm
from django.test import Client
from registerevent.models import EventRegis
from django.core.serializers import serialize


# Create your tests here.

class LandingPageTest(TestCase):

    def test_home_url_exist(self):
        #test url '/' exist
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_calling_landing_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.landing)

    def test_home_using_landing_page(self):
        #test url '/' will using landing page template
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_landing_view(self):
        request = HttpRequest()
        response = views.landing(request)
        self.assertContains(response, 'Event.in')

    def test_using_right_staticfiles(self):
        response = self.client.get('/')
        self.assertContains(response, 'static/css/stylelanding')
        self.assertContains(response, 'static/css/base')

    def test_feedback_model(self):
        obj_create =  Feedback.objects.create(feedback_message = "so good")
        feedbacks = Feedback.objects.all()

        self.assertEqual(len(feedbacks), 1)
        self.assertEqual(feedbacks[0].feedback_message, "so good")

        self.assertEqual(obj_create, feedbacks[0])

    def test_form(self):
        form = FeedbackForm(data={'feedback_message':"MANTAP"})

        self.assertTrue(form.is_valid())

        form.save()
        obj = Feedback.objects.get(id=1)
        self.assertNotEqual(obj, None)
        self.assertEqual(obj.feedback_message, "MANTAP")

    def test_post(self):
        c = Client()
        response = c.post('/', {'feedback_message':'MANTULLL'})

        feedback = Feedback.objects.all()[0]
        self.assertEqual(feedback.feedback_message, "MANTULLL")

    def test_search(self):
        EventRegis.objects.create(
            location = "jkt",
            corporate_name = "corporate",
            address = "address",
            email = "email@email.com",
            event = "event",
            tanggal = "2019-9-23",
            waktu = "10:00",
            desc = "description yes")
        
        client = Client()
        # search with event name
        response = client.get('/get/event?search=event')
        self.assertContains(response, 'jkt')
        self.assertContains(response, 'event')
        self.assertContains(response, 'description yes')
        # search with event corporation name
        response = client.get('/get/event?search=corporate')
        self.assertContains(response, 'jkt')
        self.assertContains(response, 'event')
        self.assertContains(response, 'description yes')
        # search with event description
        response = client.get('/get/event?search=yes')
        self.assertContains(response, 'jkt')
        self.assertContains(response, 'event')
        self.assertContains(response, 'description yes')
        # search with event location
        response = client.get('/get/event?search=kt')
        self.assertContains(response, 'jkt')
        self.assertContains(response, 'event')
        self.assertContains(response, 'description yes')
        # search no result
        response = client.get('/get/event?search=noevent')
        self.assertNotContains(response, 'jkt')
        self.assertNotContains(response, 'event')
        self.assertNotContains(response, 'description yes')
        self.assertEqual( [], eval(eval(response.content))) #empty list



        



        
